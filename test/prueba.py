import time


export let options = {
    duration: '1m',
    vus: 50,
    thresholds: {
        duration: ['p(95)<500'],
    },
};

export default function () {
    print("Printed immediately.")
    time.sleep(2.4)
    print("Printed after 2.4 seconds.")
}
